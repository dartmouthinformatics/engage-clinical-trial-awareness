class RemoveSelfFromProfiles < ActiveRecord::Migration
  def change
    remove_column :profiles, :self
  end
end
