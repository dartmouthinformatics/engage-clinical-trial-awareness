class ChangeCategoryColumns < ActiveRecord::Migration
  def change
    remove_column :profiles, :category_1
    remove_column :profiles, :category_2
    remove_column :profiles, :category_3
    add_column :profiles, :csv_categories, :string
  end
end
