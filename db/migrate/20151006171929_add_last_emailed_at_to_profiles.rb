class AddLastEmailedAtToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :last_emailed_at, :datetime
  end
end
