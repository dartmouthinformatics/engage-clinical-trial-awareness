class AddUuidToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :uuid, :string
  end
end
