class ChangeProfileAgeColumns < ActiveRecord::Migration
  def change
    remove_column :profiles, :adults
    remove_column :profiles, :minors
    add_column :profiles, :age, :string
  end
end
