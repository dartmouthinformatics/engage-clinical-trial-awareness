class CreateTrialNotifications < ActiveRecord::Migration
  def change
    create_table :trial_notifications do |t|
      t.integer :profile_id, null: false
      t.integer :dartmouth_clinical_trial_id, null: false
      t.timestamps null: false
    end
  end
end
