# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160802185048) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "dartmouth_clinical_trials", force: :cascade do |t|
    t.text     "category"
    t.boolean  "verified",                     default: false
    t.datetime "created_at"
    t.text     "url"
    t.text     "nct_id"
    t.text     "title"
    t.text     "sponsor"
    t.text     "summary"
    t.text     "status"
    t.date     "start_date"
    t.date     "end_date"
    t.text     "end_date_status"
    t.text     "primary_outcome"
    t.text     "condition_1"
    t.text     "condition_2"
    t.text     "condition_3"
    t.text     "condition_4"
    t.text     "condition_5"
    t.text     "gender"
    t.text     "min_age"
    t.text     "max_age"
    t.integer  "min_age_sec",        limit: 8
    t.integer  "max_age_sec",        limit: 8
    t.boolean  "healthy_volunteers"
    t.text     "eligibility"
    t.text     "study_offical_name"
    t.text     "contact_name"
    t.text     "contact_phone"
    t.text     "contact_email"
    t.text     "alt_contact_name"
    t.text     "alt_contact_phone"
    t.text     "alt_contact_email"
    t.text     "keyword_1"
    t.text     "keyword_2"
    t.text     "keyword_3"
    t.text     "keyword_4"
    t.text     "keyword_5"
    t.text     "mesh_term_1"
    t.text     "mesh_term_2"
    t.text     "mesh_term_3"
    t.text     "mesh_term_4"
    t.text     "mesh_term_5"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "profiles", force: :cascade do |t|
    t.boolean  "healthy"
    t.string   "free_text"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "active",          default: true
    t.datetime "last_emailed_at"
    t.string   "csv_categories"
    t.string   "age"
    t.string   "email"
    t.string   "uuid"
  end

  create_table "trial_notifications", force: :cascade do |t|
    t.integer  "profile_id",                  null: false
    t.integer  "dartmouth_clinical_trial_id", null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
