source 'https://rubygems.org'

gem 'haml'
gem 'devise'
gem 'awesome_print'
gem 'rspec-rails'
gem 'figaro'
gem 'pg_search'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.7'
# Use an older version of sprockets because of a less-rails incompatibility
gem 'sprockets', '3.6.3'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer'

# Use FactoryGirl for test fixtures
gem 'factory_girl_rails', :require => false
# For cleaning the db during tests
gem 'database_cleaner'
# For creating fake data in FactoryGirl
gem 'faker'
# Test coverage
gem 'simplecov', :require => false, :group => :test

# Use newrelic for performance profiling
gem 'newrelic_rpm'
# User delayed_job as a job queue
gem 'delayed_job_active_record'
gem 'daemons'
gem 'capistrano3-delayed-job'
# For parsing max and minimum eligibility ages
gem 'chronic_duration'
# Use bootstrap
gem 'twitter-bootstrap-rails'
gem 'bootstrap_form'
# Needed by twitter-bootstrap-rails
gem 'less-rails'
# Table sorting
gem 'jquery-tablesorter'
# Use font-awesome icons
gem 'font-awesome-rails'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
gem 'capistrano'
gem 'capistrano-passenger'
gem 'capistrano-bundler'
gem 'capistrano-rails'
gem 'capistrano-rbenv'

gem 'mailcatcher'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
end

group :development do
  # favicon generator
  gem 'rails_real_favicon'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

