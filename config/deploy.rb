set :delayed_job_command, 'bin/delayed_job'
set :application, 'ctvolunteers'
set :repo_url, "git@bitbucket.org:dartmouthinformatics/engage.git"
set :linked_files, %w{config/database.yml config/application.yml config/secrets.yml }
set :linked_dirs, %w{backups log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end
  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end

task :reset_db do
  on roles(:app) do
    ask :confirm, "  Are you sure you want to reset the database? (Y) "
    if ['Y', 'y', 'yes'].include? fetch(:confirm)
      # invoke 'backup_db'
      execute "sudo service nginx stop"
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'db:reset'
        end
      end
      execute "sudo service nginx start"
    end
  end
end

task :backup_db do
  on roles(:app) do
    within release_path do
      with rails_env: fetch(:rails_env) do
        execute :rake, 'db:dump'
      end
    end
  end
end
