Rails.application.routes.draw do

  devise_for :users, :controllers => {:registrations => 'registrations'}
  
  # Landing page and browsing
  root 'home#index'
  get 'browse',                           to: 'home#browse',                                  as: 'browse'
  get 'browse_healthy',                   to: 'home#browse_healthy',                          as: 'browse_healthy'
  get 'browse_with_query/:query',         to: 'home#browse_with_query',                       as: 'browse_with_query'
  get 'browse_category/:category',        to: 'home#browse_category',                         as: 'browse_category'
  get 'choose_category',                  to: 'home#choose_category',                         as: 'choose_category'
  get 'learn_more',                       to: 'home#learn_more',                              as: 'learn_more'

  # PDFs
  get 'research_participation',           to: 'home#research_participation_pdf',              as: 'research_participation'
  get 'research_participation_spanish',   to: 'home#research_participation_spanish_pdf',      as: 'research_participation_spanish'
  get 'bill_of_rights',                   to: 'home#bill_of_rights_pdf',                      as: 'bill_of_rights'
  get 'blood_draw',                       to: 'home#blood_draw_pdf',                          as: 'blood_draw'
  get 'blood_draw_spanish',               to: 'home#blood_draw_spanish_pdf',                  as: 'blood_draw_spanish'
  get 'genetic_research',                 to: 'home#genetic_research_pdf',                    as: 'genetic_research'
  get 'genetic_research_spanish',         to: 'home#genetic_research_spanish_pdf',            as: 'genetic_research_spanish'

  # Profile creation
  get  'age',                             to: 'profiles#get_age',                             as: 'age'
  post 'save_age',                        to: 'profiles#save_age',                            as: 'save_age'
  get  'healthy',                         to: 'profiles#get_healthy',                         as: 'healthy'
  post 'save_healthy',                    to: 'profiles#save_healthy',                        as: 'save_healthy'
  get  'categories',                      to: 'profiles#get_categories',                      as: 'categories'
  post 'save_categories',                 to: 'profiles#save_categories',                     as: 'save_categories'
  get  'email',                           to: 'profiles#get_email',                           as: 'email'
  post 'save_email',                      to: 'profiles#save_email',                          as: 'save_email'
  get 'profile_complete/:uuid',           to: 'profiles#profile_complete',                    as: 'profile_complete'
  get 'unsubscribe/:uuid',                to: 'profiles#unsubscribe',                         as: 'unsubscribe'

  # Admin
  post 'update_verified/:id',             to: 'admin#update_verified',                        as: 'update_verified'
  post 'update_category/:id',             to: 'admin#update_category',                        as: 'update_category'
  get 'edit_trials',                      to: 'admin#edit_trials',                            as: 'edit_trials'
  get 'edit_trial/:id',                   to: 'dartmouth_clinical_trials#edit',               as: 'edit_trial'
  get 'show_trial/:id',                   to: 'dartmouth_clinical_trials#show',               as: 'show_trial'
  post 'update_trial',                    to: 'dartmouth_clinical_trials#update',             as: 'update_trial'
  get 'admin',                            to: 'admin#index',                                  as: 'admin'  
  get 'search',                           to: 'admin#search',                                 as: 'search'
  post 'search_results',                  to: 'admin#search_results',                         as: 'search_results'
  post 'user_email',                      to: 'admin#user_email',                             as: 'user_email'

end
