set :rails_env, 'production'
set :stage, :production
set :branch, 'master'
set :branch, ENV['BRANCH'] if ENV['BRANCH']
set :deploy_to, '/home/deploy/ctvolunteers'
server 'ctvolunteers-srv.dartmouth.edu', user: 'deploy', roles: %w(app db)
