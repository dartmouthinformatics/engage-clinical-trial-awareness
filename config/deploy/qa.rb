set :rails_env, 'qa'
set :stage, :qa
set :branch, 'master'
set :deploy_to, '/home/deploy/ctvolunteers'
server 'ctv-dev-srv.dartmouth.edu', user: 'deploy', roles: %w(app db)
