class Profile < ActiveRecord::Base

  has_many :trial_notifications

  validates_uniqueness_of :email
  validates :csv_categories, :age, :email, :uuid, presence: true
  validates :healthy, :active, inclusion: [true, false]

  scope :active, -> { where(active: true) }

  # All dartmouth_clinical_trials that match this profile
  # NOTE: Slick but brittle. Profile.age values much have a corresponding scope in the DartmouthClinicalTrial model
  def trials
    if healthy
      DartmouthClinicalTrial.recruiting.send(age).where("category in (?) or healthy_volunteers=true", categories).all
    else
      DartmouthClinicalTrial.recruiting.send(age).where(category: categories).all
    end
  end

  # All dartmouth_clinical_trials that both match this profile and have not had a notification sent
  def trials_needing_notification
    trials.where.not(id: TrialNotification.select('dartmouth_clinical_trial_id').where(profile_id: id)).all
  end

  # Returns an array of category strings. Should all be members of DartmouthClinicalTrial.categories
  def categories
    csv_categories.split(',')
  end

end
