class User < ActiveRecord::Base

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  scope :admins, -> { where("admin = ?", true) }
  # NOTE: This scope is not currently used.
  # Volunteers do not have User accounts.
  # All users are admins
  scope :volunteers, -> { where("admin = ?", false) }

  def admin?
    admin
  end

end
