class TrialNotification < ActiveRecord::Base

  belongs_to :profile
  belongs_to :dartmouth_clinical_trial

end
