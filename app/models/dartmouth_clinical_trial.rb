class DartmouthClinicalTrial < ActiveRecord::Base

  has_many :trial_notifications

  include PgSearch
  pg_search_scope :search, against: [:category,
                                     :title,
                                     :summary,
                                     :condition_1,
                                     :condition_2,
                                     :condition_3,
                                     :condition_4,
                                     :condition_5,
                                     :keyword_1,
                                     :keyword_2,
                                     :keyword_3,
                                     :keyword_4,
                                     :keyword_5,
                                     :mesh_term_1,
                                     :mesh_term_2,
                                     :mesh_term_3,
                                     :mesh_term_4,
                                     :mesh_term_5]

  scope :recruiting, -> { where(status: 'Recruiting') } 
	scope :healthy, -> { where(healthy_volunteers: true)}
  scope :adults, -> { where("max_age_sec >= 567720000 OR max_age_sec IS NULL") }
  scope :minors, -> { where("min_age_sec < 567720000 OR min_age_sec IS NULL") }
  # We want to return all records in this case
  scope :both, -> { where("id > 0") }
  scope :verified, -> { where(verified: true) }
  # NOTE: Do not include any categories with commas in them.
  #       Commas are used as a delimiter in when storing
  #       categories in a Profile record.
  def self.categories
  ['Addiction/Substance Abuse',
  'Aging/Seniors',
  'Blood Heart and Circulation',
  'Bones Joints and Muscles',
  'Brain Spinal Cord and Nervous System',
  'Cancers',
  'Children and Teenagers',
  'Complementary and Alternative Therapies',
  'Diabetes Mellitus',
  'Diagnostic Tests',
  'Digestive System/Gastrointestinal',
  'Disasters',
  'Drug Therapy',
  'Ear Nose and Throat',
  'Endocrine System',
  'Female Reproductive System',
  'Fitness and Exercise',
  'Food and Nutrition',
  'Genetics/Birth Defects',
  'Infants/Newborns',
  'Immune System',
  'Infections',
  'Injuries and Wounds',
  'Kidneys and Urinary System',
  'Lungs and Breathing',
  'Male Reproductive System',
  'Men’s Health',
  'Mental Health and Behavior',
  'Metabolic Problems',
  'Mouth and Teeth',
  'Obesity and Weight Control',
  'Personal Health Issues',
  'Population Groups',
  'Pregnancy and Reproduction',
  'Safety Issues',
  'Sexual Health Issues',
  'Skin Hair and Nails',
  'Sleep Disorders',
  'Surgery and Rehabilitation',
  'Wellness and Lifestyle',
  'Women’s Health']
  end
end
