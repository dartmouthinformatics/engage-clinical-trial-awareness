class UserMailer < ApplicationMailer

  def new_profile(profile)
    @profile = profile
    mail(to: @profile.email, subject: 'Confirmation for engage.dartmouth.edu')
  end

  def trial_notification(profile, trials)
    @profile = profile
    @trials = trials
    subject = 'A clinical trial participation opportunity from engage.dartmouth.edu'
    subject = 'Clinical trial participation opportunities from engage.dartmouth.edu' if @trials.length > 1
    mail(to: @profile.email, subject: subject)
  end

end
