class AdminMailer < ApplicationMailer

  def user_added
    to = User.admins.map {|u| u.email}
    mail(to: to, subject: 'New clinical trials volunteer registration')
  end

end
