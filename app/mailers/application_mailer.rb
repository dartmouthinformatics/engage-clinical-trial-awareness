class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@engage.dartmouth.edu"
  layout 'mailer'
end
