class AdminController < ApplicationController

  before_action :only_admins

  # Dashboard (landing page) for admins
  def index
    @trials = DartmouthClinicalTrial.recruiting
    @profiles = Profile.active.order(:email)
  end

  # Lists all recruiting trials and has search box
  def search
    @trials = DartmouthClinicalTrial.recruiting
  end

  # Renders search results from search method
  def search_results
    @search = true
    @query = params[:query]
    @trials = DartmouthClinicalTrial.search(@query)
    render 'search'
  end

  # Lists all trials and allows category editing/confirmation
  def edit_trials
    @trials = DartmouthClinicalTrial.recruiting.order('created_at DESC, title ASC')
    @categories = DartmouthClinicalTrial.categories
  end

  # Called via Ajax from the edit_trials page
  def update_verified
    trial = DartmouthClinicalTrial.find(params[:id])
    if trial.update_attributes(verified: params[:verified])
      render json: 'success'
    else
      render json: 'error'
    end
  end

  # Called via Ajax from the edit_trials page
  def update_category
    trial = DartmouthClinicalTrial.find(params[:id])
    if trial.update_attributes(category: params[:category])
      render json: 'success'
    else
      render json: 'error'
    end
  end

end
