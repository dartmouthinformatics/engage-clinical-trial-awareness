class ProfilesController < ApplicationController

  skip_before_filter :auth_user

  # Form asking age range preference
  def get_age
    session['profile'] = {} unless session['profile'].present?
    @age = session['profile']['age']
  end

  # Store age range preference in session
  def save_age
    if profile_params[:age].present?
      session['profile']['age'] = profile_params[:age]
      redirect_to healthy_path
    else
      redirect_to age_path
    end
  end

  # Form asking healthy volunteers question
  def get_healthy
    redirect_to age_path unless session['profile']['age'].present?
    @healthy = session['profile']['healthy']
  end

  # Store healthy volunteers preference in session
  def save_healthy
    if profile_params[:healthy].present?
      session['profile']['healthy'] = profile_params[:healthy]
      redirect_to categories_path
    else
      redirect_to healthy_path
    end
  end

  # Form asking trial categories question
  def get_categories
    redirect_to healthy_path unless session['profile']['healthy'].present?
    @categories = session['profile']['categories']
    # Splitting categories for two-column display
    categories = DartmouthClinicalTrial.categories
    @categories_first_half = categories[0, categories.length / 2]
    @categories_second_half = categories[categories.length / 2, categories.length]
  end

  # Store trial category preferences in session
  def save_categories
    # Can't use strong parameters here without duplicating the whole category list
    # So, test each key for inclusion in the category list before adding it
    if params[:categories].present?
      all_categories = DartmouthClinicalTrial.categories
      categories = [] 
      params[:categories].keys.each do |category|
        categories << category if all_categories.include?(category)
      end
      session['profile']['categories'] = categories
      redirect_to email_path
    else
      redirect_to categories_path
    end
  end

  # Form asking for profile email address
  def get_email
    redirect_to categories_path unless session['profile']['categories'].present?
    @email = session['profile']['email']
  end

  # Store profile email in session and redirect to create profile record
  def save_email
    if profile_params[:email].present?
      session['profile']['email'] = profile_params[:email]
      create
    else
      redirect_to email_path
    end
  end

  # Displayed upon successful profile creation
  def profile_complete
    @profile = Profile.where(uuid: params[:uuid]).first
    @trials = @profile.trials
    UserMailer.new_profile(@profile).deliver_later!(wait: 10.seconds)
  end

  # Unsubscribe profile from future emails
  def unsubscribe
    profile = Profile.where(uuid: params[:uuid]).first
    profile.update_attributes(active: false) if profile
    # Redirect back if profile was unsubscribed by an admin
    if current_user and current_user.admin?
      flash.notice = "#{profile.email} has been unsubscribed"
      redirect_to :back
      return
    end
  end

  private

  def profile_params
    params.permit(
      :age,
      :healthy,
      :email
    )
  end

  # Create profile record
  def create
    profile = Profile.new
    profile.age = session['profile']['age']
    profile.healthy = {'yes': true, 'no': false}[session['profile']['healthy'].to_sym] if session['profile']['healthy']
    profile.email = session['profile']['email']
    profile.csv_categories = session['profile']['categories'].join(',')
    profile.uuid = SecureRandom.hex(30)
    if profile.save
      # Successful save. Clear the profile parameters from the session
      session['profile'] = {}
      redirect_to profile_complete_path(uuid: profile.uuid)
    else
      flash.alert = profile.errors.full_messages
      redirect_to email_path
    end
  end

end
