class HomeController < ApplicationController

  skip_before_filter :auth_user

  def index
    if current_user.try(:admin?)
      redirect_to admin_path
    end
    @faqs = faqs
  end

  def browse
    @trials = browse_trials
  end

  def browse_healthy
    @trials = browse_trials
		@healthy = true
    render :browse
  end

  def browse_with_query
    @trials = browse_trials
    # If coming in from the landing page, there will be a passed-in query
    @query = ActionController::Base.helpers.sanitize params[:query]
    render :browse
  end

	def browse_category
    @trials = browse_trials
    @category = ActionController::Base.helpers.sanitize params[:category]
    render :browse
	end

  def choose_category
    @categories = DartmouthClinicalTrial.verified.recruiting.select(:category).distinct.order(:category).map {|trial| trial.category}
  end

  def learn_more
  end

  def research_participation_pdf
    send_pdf 'research_participation'
  end

  def research_participation_spanish_pdf
    send_pdf 'research_participation_spanish'
  end

  def bill_of_rights_pdf
    send_pdf 'bill_of_rights'
  end

  def blood_draw_pdf
    send_pdf 'blood_draw'
  end

  def blood_draw_spanish_pdf
    send_pdf 'blood_draw_spanish'
  end

  def genetic_research_pdf
    send_pdf 'genetic_research'
  end

  def genetic_research_spanish_pdf
    send_pdf 'genetic_research_spanish'
  end

  private

  def send_pdf(filename)
    send_file "#{Rails.root}/public/pdf/#{filename}.pdf", disposition: 'inline'
  end

  def browse_trials
    DartmouthClinicalTrial.recruiting.verified.order('created_at DESC, title ASC')
  end

  # TODO: Don't store these here
  def faqs
    [
      {
        question: "What is a clinical trial?",
          answer: "Clinical trials are experiments or observations done in clinical research. Such prospective biomedical or behavioral research studies on human participants are designed to answer specific questions about biomedical or behavioral interventions, including new treatments (such as novel vaccines, drugs, dietary choices, dietary supplements, and medical devices) and known interventions that warrant further study and comparison."
      },
      {
        question: "Why would I want to participate in a clinical trial?",
          answer: "People have different reasons. Mostly, It's the money."
      },
      {
        question: "Is this another question about clinical trials?",
          answer: "Yes, it most certainly is. You may find that there are even more questions below."
      },
      {
        question: "Does this seem to be the last question about clinical trials?",
          answer: "Absolutely. That is how things seem."
      },
      {
        question: "What is a clinical trial?",
          answer: "It's a pretty cool thing. Most people don't know about it."
      },
      {
        question: "What is a clinical trial?",
          answer: "It's a pretty cool thing. Most people don't know about it."
      },
      {
        question: "What is a clinical trial?",
          answer: "It's a pretty cool thing. Most people don't know about it."
      },
      {
        question: "What is a clinical trial?",
          answer: "It's a pretty cool thing. Most people don't know about it."
      }
    ]
  end

end
