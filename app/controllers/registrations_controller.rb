# This is an override of Devise's RegistrationsController


class RegistrationsController < Devise::RegistrationsController
   
  skip_before_filter :auth_user
  #before_filter :check_profile_params, only: [:new]

  def new
    @profile_id = params[:id]
    #Sanitizing the parameters. Have to post these while calling registration_controller. 
    
    # Check params, make sure they are valid
    # TODO : Need to configure the error paths. 

    if !check_profile_params
        redirect_to root_path and return
    end
    profile_params = params.require(:profile).permit(:id, :user_id, :self, :adults, :minors, :healthy, :free_text)
    @profile = Profile.new(profile_params)
    super
  end

  def create

    # Check params, make sure they are valid
    # TODO : Need to configure the error paths. 
    if !check_params
        redirect_to root_path,:profile[:adults] => params[:profile_adults] and return
    end
    super
    # Get details from profile page. 
    user = User.find_by_email(params[:user][:email])
    Profile.create!(:adults => params[:profile_adults], :minors => params[:profile_minors], :healthy => params[:profile_healthy], :self => true, :user_id => user.id)

    # Send email to all admins notifying them of the new volunteer
    AdminMailer.user_added.deliver_now
  end

  def update
    super
  end

  private

  def after_sign_up_path_for(resource)
    thank_you_path
  end

  # Override the default sign_up method so that the new user does not get logged in
  def sign_up(resource_name, resource)
    true
  end

  # Check if all values are present in the profile being posted.
  def check_profile_params
      if ((params[:profile][:adults] or params[:profile][:minors]) and  params[:profile][:healthy])
          return true
      else
          return false
      end
  end

  # Check if all values are present, again. Difference is that I am not passing the entire structure like above.  
  def check_params
      if ((params[:profile_adults] or [:profile_minors]) and  params[:profile_healthy])
          return true
      else
          return false
      end
  end

end
