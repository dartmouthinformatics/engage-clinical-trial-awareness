class DartmouthClinicalTrialsController < ApplicationController

  before_action :only_admins

  # Show details for a single trial
  def show
    @trial = DartmouthClinicalTrial.find(params[:id])
  end

  # Display edit form for a single trial
  def edit
    @trial = DartmouthClinicalTrial.find(params[:id])
  end

  # Save edits to a trial
  def update
    @trial_params = trial_params
    @trial = DartmouthClinicalTrial.find(@trial_params[:id]) 
    if @trial.update_attributes(@trial_params)
      flash.notice = 'Successfully Edited Trial'
      redirect_to :edit_trials
    else
      flash.now.alert = @trial.errors.full_messages.join("\n").html_safe
    end
  end

  private

  # Params whitelist
  def trial_params
    params.require(:dartmouth_clinical_trial).permit(
      :id,
      :title,
      :summary
    )
  end

end
