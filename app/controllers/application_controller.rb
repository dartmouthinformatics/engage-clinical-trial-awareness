class ApplicationController < ActionController::Base

  # Prevent CSRF attacks by raising an exception.
  protect_from_forgery with: :exception

  # Authenticate for all actions by default
  # before_action :auth_user

  def auth_user
    redirect_to new_profile_path unless (user_signed_in? or params[:controller].include? 'devise')
  end

  def only_admins
    redirect_to root unless current_user && current_user.admin?
  end

end
