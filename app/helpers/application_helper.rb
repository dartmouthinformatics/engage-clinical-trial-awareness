module ApplicationHelper

	def star
		content_tag :span, '*', style: 'color:red;'
	end

  def category_count(category)
    return nil unless category
    DartmouthClinicalTrial.recruiting.where(category: category).count
  end

end
