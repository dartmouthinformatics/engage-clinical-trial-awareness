$(function() {

  // Show submit buttons that require something to be checked on the page
  // upon page load
  if($(':checked').length > 0){ $('.conditional-submit-display').show() };

  // Show continue button when self/other question answered
  $('.self-other-radio-button').change(function() {
    $('#self-other-submit').show();
  })

  // Show continue button when age question answered
  $('.age-radio-button').change(function() {
    $('#age-submit').show();
  })

  // Show continue button when healthy question answered
  $('.healthy-radio-button').change(function() {
    $('#healthy-submit').show();
  })

  $('.category-checkbox').change(function(){
    categoryCheckboxes();
  })

  $('#profile-email').keyup(function(){
    if(validateEmail($(this).val())){
      $('#email-submit').show();
    } else {
      $('#email-submit').hide();
    } 
  })

})

// Show complete signup button when >= 1 category is chosen
// Disable unchecked boxes when 3 boxes are checked
function categoryCheckboxes() {
  var numChecked = 0;
  $('.category-checkbox').each(function() {
    if(this.checked) {
      numChecked += 1;
    }
  });
  if(numChecked == 0){
    $('#categories-submit').hide();
  }
  if(numChecked > 0){
    $('#categories-submit').show();
  }
}

// http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
