$(function() {

  // Filter trials based on search query
  $('#query').keyup(function() {
		filterNow();
  })

  // Searching from landing page
  $('#landing-page-query').on('keyup', function (e) {
		// Enter key keyup
    if (e.keyCode == 13) {
      var hiddenLink = $('#query-hidden-link');
			var origHref = hiddenLink.attr('href');
      var query = $('#landing-page-query').val();
			window.location.href = origHref.replace('placeholder', query);
    }
  });

	// Disable fixed position footer on mobile
	var mediaQuery = window.matchMedia( '(max-width: 768px)' );
	if(mediaQuery.matches){
		// Mobile
		$('#footer').removeClass('navbar-fixed-bottom');
  } else {
		// Larger
		$('#footer').addClass('navbar-fixed-bottom');
  }

  // Click hidden link on choose_category page
  $('.category-link-container').click(function(){
    var link = $(this).find('.hidden-link');
    window.location  = link.attr('href');
  })
	
})
