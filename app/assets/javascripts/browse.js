$(function() {

  // Handle clicks on Healthy Volunteers filter
  $('#btn-healthy').click(function(e) {
    var checkBox = $('#healthy-check-box');
    if(checkBox.hasClass('fa-square-o')) {
      // Turn on the healthy volunteers filter
      checkBox.removeClass('fa-square-o').addClass('fa-check-square-o');
			filterOnlyHealthy = true;
    } else {
      // Turn off the healthy volunteers filter
      checkBox.removeClass('fa-check-square-o').addClass('fa-square-o');
			filterOnlyHealthy = false;
    }
    filterNow();
  })

  // Handles selections in category filter
  $('.category-dropdown-link').click(function() {
    $('#current-category').text($(this).text());
    currentCategory = $(this).attr('name');
    filterNow();
  })

  // Clicks on category badges in trial listings
  $('.category-badge').click(function(e) {
    $('#current-category').text($(this).text());
    currentCategory = $(this).text();
    filterNow();
  })

  // Clicks on Healthy Volunteers badges in trial listings
  $('.healthy-badge').click(function(e) {
    $('#healthy-check-box').focus().blur();
    $('#healthy-check-box').removeClass('fa-square-o').addClass('fa-check-square-o');
		filterOnlyHealthy = true;
    filterNow();
  })
  

  // Add sort to main browse table
  $('.sorted-table').tablesorter({
    theme: 'default',
    cssChildRow: 'trial-details'
  });

  // Show and hide trial details
  $('.trial').click(function(e) {
    var titleRow = $(e.target).closest('.title-row');
    var details = titleRow.next('.trial-details');
    if(details.is(':visible')){
      showHideTrials(titleRow, details, true)
    } else {
      showHideTrials(titleRow, details, false)
    }
  })

  // On browse page, focus on search box on page load
  $('#query').focus();

})

// Hide all trial details
function hideAllTrialDetails() {
  showHideTrials($('.title-row'), $('.trial-details'), true)
}

// Show/Hide trial details
function showHideTrials(titleRows, detailRows, hide){
  if(hide){
    detailRows.hide();
    detailRows.children().removeClass('highlight');
    titleRows.children().removeClass('highlight no-bottom-border')
    titleRows.find('.trial-chevron').removeClass('fa-chevron-circle-down').addClass('fa-chevron-circle-right');
  } else {
    detailRows.show();
    detailRows.children().addClass('highlight');
    titleRows.children().addClass('highlight no-bottom-border')
    titleRows.find('.trial-chevron').removeClass('fa-chevron-circle-right').addClass('fa-chevron-circle-down');
  }
}

// Assign query and run table filter
function filterNow(){
  var query = $('#query').val();
  var rows = $('.title-row');
	hideAllTrialDetails();
  tableFilter(rows, query);
}

// Case-insensitive table row filtering
// Also applies category and healthy volunteer filters when appropriate
function tableFilter(rows, query){
  var filterMatches = 0;
  var splitQuery = query.split(" ");
  var numTokens = splitQuery.length;
  // Hide all the rows
  rows.hide();
  // Recusively filter the jQuery object to get results.
  rows.filter(function (i, v) {
    var $t = $(this);
    var tokenMatches = 0;
    for (var d = 0; d < splitQuery.length; ++d) {
      if ($t.text().toUpperCase().indexOf(splitQuery[d].toUpperCase()) > -1) {
        tokenMatches += 1;
      }
    }
    if(tokenMatches == numTokens || query == ""){
      // Search tokens matched. Check other filters
			var categoryOk = false;
			var healthyOk = false;
      if(currentCategory == 'All'){
        categoryOk = true
      } else {
        // Apply category filter
        if($t.find('.category-badge').text() == currentCategory){
          categoryOk = true
        }
      }
			if(filterOnlyHealthy) {
        // Apply healthy volunteers only filter
				if($t.find('.healthy-badge').get(0)) {
					healthyOk = true;
				}
			} else { 
				healthyOk = true;
			}
			if(categoryOk && healthyOk) {
				filterMatches += 1;
				return true;
			} else {
				return false;
			}
    }
    return false
  }).show();
  $('#matches').text(filterMatches);
}
