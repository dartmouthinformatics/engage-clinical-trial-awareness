$(function() {
  $('.category-select').each(function() {
    var checkBox = $(this).parent().parent().find('.verified-check-box');
    var verified = checkBox.prop('checked');
    if(verified){
      $(this).prop('disabled', true);
    }
  })
})

$(function() {
  $('.verified-check-box').change(function(e) {
    var checkBox = $(e.target);
    var trialId = checkBox.prop('id');
    var verified = checkBox.prop('checked');
    var selectBox = checkBox.parent().parent().find('select');
    if(verified){
      selectBox.prop('disabled', true);
    } else {
      selectBox.prop('disabled', false);
    }
    var url = '/update_verified/' + trialId;
    $.ajax({
      type: "POST",
      url: url,
      data: { 'verified': verified }
    });
  })
})

$(function() {
  $('.category-select').change(function(e) {
    var select_box = $(e.target);
    var trialId = select_box.prop('id');
    var category = select_box.val();
    var url = '/update_category/' + trialId;
    $.ajax({
      type: "POST",
      url: url,
      data: { 'category': category }
    });
  })
})

$(function() {
  $('.triangle').click(function() {
    if ( $(this).hasClass('revealed') ) {
      hideProfileRow($(this));
    } else {
      showProfileRow($(this));
    }
  })
})

$(function() {
  $('.trials-count').click(function(e) {
    e.preventDefault();
    var trialLinks = $(this).siblings('.profile-trials')
    trialLinks.slideToggle();
  })
})

function hideProfileRow(link){
  link.removeClass('revealed');
  link.html("&#9658;");
  link.parent().parent().next('.profile-detail').slideUp();
}

function showProfileRow(link){
  link.addClass('revealed');
  link.html("&#9660;");
  link.parent().parent().next('.profile-detail').slideDown();
}
