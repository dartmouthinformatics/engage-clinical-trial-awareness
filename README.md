### Engage application build instructions
1. Get the Rails application running
  1. Clone the repository from https://bitbucket.org/dartmouthinformatics/engage
  2. Run _bundle install_ and address any system dependency issues
  3. Create a local config/application.yml file including the following keys:
    * secret_key_base (create with _rake secret_)
    * mail_default_from (any email address)
    * mail_address (probably _localhost_)
    * host (probably _localhost:3000_)
  4. Create a local config/database.yml file to configure the pg connection http://edgeguides.rubyonrails.org/configuring.html#configuring-a-database
  5. Install PostgreSQL https://www.postgresql.org/download/
  6. In psql, create the application's database user as specified in database.yml
  7. In psql, create the application's database and make it owned by the application's database user
  8. Run _bundle exec rake db:reset_ to initialize the database
  9. Run _bundle exec rails server_ to start the local webserver
2. Run the Python import script to populate DartmouthClinicalTrials
  1. Navigate to /lib/trials/import and create a virtualenvironment using requirements.txt
  2. Activate the virtualenvironment and run _python import.py_
