require "rails_helper"

RSpec.describe AdminMailer, type: :mailer do
  
  describe '#user_added' do
    before do
      @admin = build(:admin)
    end
    it 'sends an email to each admin' do
      @admin.save
      mail = AdminMailer.user_added.deliver_now!
      expect(mail.to.first).to eq(@admin.email)
    end
    it 'has particular content in the email body' do
      @admin.save
      mail = AdminMailer.user_added.deliver_now!
      expect(ActionMailer::Base.deliveries.last.to_s).to include('This automated message is from the Dartmouth Clinical Trials Volunteers application.')
    end
  end

end
