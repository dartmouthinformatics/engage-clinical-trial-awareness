require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  
  describe '#new_profile(profile)' do
    before do
      @profile = build(:profile)
    end
    it 'sends to the profile email' do
      mail = UserMailer.new_profile(@profile).deliver_now!
      expect(mail.to.first).to eq(@profile.email)
    end
    it 'has particular content in the email body' do
      mail = UserMailer.new_profile(@profile).deliver_now!
      expect(ActionMailer::Base.deliveries.last.to_s).to include('You will receive occasional emails containing information')
    end
  end
  
  describe '#trial_notification(profile, trials)' do
    before do
      @profile = build(:profile)
      @trial = build(:recruiting_trial)
    end
    it 'sends to the profile email' do
      @trial.save
      mail = UserMailer.trial_notification(@profile, [@trial]).deliver_now!
      expect(mail.to.first).to eq(@profile.email)
    end
    it 'has particular content in the email body' do
      mail = UserMailer.trial_notification(@profile, [@trial]).deliver_now!
      expect(ActionMailer::Base.deliveries.last.to_s).to include('If you see a trial that interests you, you can get more information by emailing the contact listed with the study.')
    end
  end

end
