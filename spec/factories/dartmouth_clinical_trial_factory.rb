FactoryGirl.define do

  factory :recruiting_trial, class: DartmouthClinicalTrial do
    status 'Recruiting'
    url { Faker::Internet.url }
    category DartmouthClinicalTrial.categories.first
  end

  factory :inactive_trial, class: DartmouthClinicalTrial do
    status 'Inactive'
    url { Faker::Internet.url }
    category DartmouthClinicalTrial.categories.first
  end

  factory :adult_trial, class: DartmouthClinicalTrial do
    status 'Recruiting'
    max_age_sec 567720000
    min_age_sec 999999999
    url { Faker::Internet.url }
    category DartmouthClinicalTrial.categories.first
  end
 
  factory :minor_trial, class: DartmouthClinicalTrial do
    status 'Recruiting'
    min_age_sec 56771999
    max_age_sec 1
    url { Faker::Internet.url }
    category DartmouthClinicalTrial.categories.first
  end
 
  factory :first_category_trial, class: DartmouthClinicalTrial do
    status 'Recruiting'
    url { Faker::Internet.url }
    category DartmouthClinicalTrial.categories.first
  end

end
