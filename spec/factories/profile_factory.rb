FactoryGirl.define do

  factory :profile do
    healthy true
    active true
    csv_categories "#{DartmouthClinicalTrial.categories[0]},#{DartmouthClinicalTrial.categories[1]}" 
    age 'both'
    email { Faker::Internet.email }
    uuid SecureRandom.hex(30)
  end

  factory :profile_healthy_false, class: Profile do
    healthy false
    active true
    csv_categories "#{DartmouthClinicalTrial.categories[0]},#{DartmouthClinicalTrial.categories[1]}" 
    age 'both'
    email { Faker::Internet.email }
    uuid SecureRandom.hex(30)
  end

  factory :profile_age_minors, class: Profile do
    healthy true
    active true
    csv_categories "#{DartmouthClinicalTrial.categories[0]},#{DartmouthClinicalTrial.categories[1]}" 
    age 'minors'
    email { Faker::Internet.email }
    uuid SecureRandom.hex(30)
  end

  factory :profile_age_adults, class: Profile do
    healthy true
    active true
    csv_categories "#{DartmouthClinicalTrial.categories[0]},#{DartmouthClinicalTrial.categories[1]}" 
    age 'adults'
    email { Faker::Internet.email }
    uuid SecureRandom.hex(30)
  end

end
