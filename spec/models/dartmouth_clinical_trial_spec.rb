require 'rails_helper'

RSpec.describe DartmouthClinicalTrial, type: :model do

  before do
    @recruiting_trial = build(:recruiting_trial)
    @inactive_trial = build(:inactive_trial)
    @adult_trial = build(:adult_trial)
    @minor_trial = build(:minor_trial)
  end

  it 'has_a_valid_factory' do
    @recruiting_trial.save
    @inactive_trial.save
    @adult_trial.save
    @minor_trial.save
    expect(DartmouthClinicalTrial.count).to eq(4)
  end

  it 'has_a_recruiting_scope' do
    @recruiting_trial.save
    @inactive_trial.save
    expect(DartmouthClinicalTrial.recruiting.count).to eq(1)
  end

  it 'has_an_adults_scope' do
    @adult_trial.save
    @minor_trial.save
    expect(DartmouthClinicalTrial.adults.count).to eq(1)
  end

  it 'has_a_minors_scope' do
    @adult_trial.save
    @minor_trial.save
    expect(DartmouthClinicalTrial.minors.count).to eq(1)
  end

  it 'has_a_both_scope' do
    @adult_trial.save
    @minor_trial.save
    expect(DartmouthClinicalTrial.both.count).to eq(2)
  end

  it 'has_a_list_of_categories' do
    expect(DartmouthClinicalTrial.categories).not_to be_empty
  end

  it 'returns_categories_as_an_array' do
    expect(DartmouthClinicalTrial.categories).to be_instance_of(Array)
  end

  it 'returns_categories_as_an_array_of_strings' do
    DartmouthClinicalTrial.categories.each do |category|
      expect(category).to be_instance_of(String)
    end
  end

  it 'returns_58_categories' do
    expect(DartmouthClinicalTrial.categories.length).to eq(58)
  end

end
