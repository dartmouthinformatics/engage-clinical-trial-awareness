require 'rails_helper'

RSpec.describe User, type: :model do

  context 'when a regular user' do

    before do
      @user = build(:user)
    end

    it 'has a valid factory' do
      expect(@user.valid?).to eq(true)
    end
    it 'can be saved' do
      expect(@user.save).to eq(true)
    end
    it 'is a normal (non-admin) user' do
      @user.save
      expect(@user.admin?).to eq(false)
    end
    
  end

  context 'when an admin' do

    before do
      @admin = build(:admin)
    end

    it 'has a valid factory' do
      expect(@admin.valid?).to eq(true)
    end
    it 'can be saved' do
      expect(@admin.save).to eq(true)
    end
    it 'is an admin user' do
      @admin.save
      expect(@admin.admin?).to eq(true)
    end
    
  end

end
