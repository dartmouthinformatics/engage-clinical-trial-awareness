require 'rails_helper'

RSpec.describe Profile, type: :model do

  before do
    @profile = build(:profile)
    @profile_healthy_false = build(:profile_healthy_false)
    @profile_age_minors = build(:profile_age_minors)
    @profile_age_adults = build(:profile_age_adults)
    @trial = build(:first_category_trial)
    @trial_minors = build(:minor_trial)
    @trial_adults = build(:adult_trial)
  end

  context 'when having a default profile' do
    it 'has_no_trials_if_none_match' do
      expect(@profile.trials.length).to eq(0)
    end
    it 'has_one_trial_if_one_matches' do
      @trial.save
      expect(@profile.trials.length).to eq(1)
    end
    it 'requires_one_notification_when_appropriate' do
      @trial.save
      expect(@profile.trials_needing_notification.length).to eq(1)
    end
    it 'requires_no_notifications_when_appropriate' do
      expect(@profile.trials_needing_notification.length).to eq(0)
    end
    it 'can_convert_csv_categories_into_an_array' do
      expect(@profile.categories).to be_instance_of(Array)
    end
  end

  context 'when having a healthy=false profile' do
    it 'has_no_trials_if_none_match' do
      expect(@profile_healthy_false.trials.length).to eq(0)
    end
    it 'has_one_trial_if_one_matches' do
      @trial.save
      expect(@profile_healthy_false.trials.length).to eq(1)
    end
    it 'requires_one_notification_when_appropriate' do
      @trial.save
      expect(@profile_healthy_false.trials_needing_notification.length).to eq(1)
    end
    it 'requires_no_notifications_when_appropriate' do
      expect(@profile_healthy_false.trials_needing_notification.length).to eq(0)
    end
    it 'can_convert_csv_categories_into_an_array' do
      expect(@profile_healthy_false.categories).to be_instance_of(Array)
    end
  end

  context 'when having an age=minors profile' do
    it 'has_no_trials_if_none_match' do
      expect(@profile_age_minors.trials.length).to eq(0)
    end
    it 'has_one_trial_if_one_matches' do
      @trial_minors.save
      @trial_adults.save
      expect(@profile_age_minors.trials.length).to eq(1)
    end
    it 'requires_one_notification_when_appropriate' do
      @trial_minors.save
      expect(@profile_age_minors.trials_needing_notification.length).to eq(1)
    end
    it 'requires_no_notifications_when_appropriate' do
      expect(@profile_age_minors.trials_needing_notification.length).to eq(0)
    end
    it 'can_convert_csv_categories_into_an_array' do
      expect(@profile_age_minors.categories).to be_instance_of(Array)
    end
  end

  context 'when having an age=adults profile' do
    it 'has_no_trials_if_none_match' do
      expect(@profile_age_adults.trials.length).to eq(0)
    end
    it 'has_one_trial_if_one_matches' do
      @trial_minors.save
      @trial_adults.save
      expect(@profile_age_adults.trials.length).to eq(1)
    end
    it 'requires_one_notification_when_appropriate' do
      @trial_adults.save
      expect(@profile_age_adults.trials_needing_notification.length).to eq(1)
    end
    it 'requires_no_notifications_when_appropriate' do
      expect(@profile_age_adults.trials_needing_notification.length).to eq(0)
    end
    it 'can_convert_csv_categories_into_an_array' do
      expect(@profile_age_adults.categories).to be_instance_of(Array)
    end
  end

end
