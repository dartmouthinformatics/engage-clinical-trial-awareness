#!/bin/bash
source /home/deploy/venv/bin/activate
cd /home/deploy/ctvolunteers/current/lib/trials_import
python /home/deploy/ctvolunteers/current/lib/trials_import/import.py
echo "$(date)" >> /home/deploy/ctvolunteers/current/log/trials_updates.log
