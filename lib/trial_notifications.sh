#!/bin/bash
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
cd /home/deploy/ctvolunteers/current/
bundle exec rake send_notifications RAILS_ENV=production
echo "$(date)" >> /home/deploy/ctvolunteers/current/log/trials_notifications.log
