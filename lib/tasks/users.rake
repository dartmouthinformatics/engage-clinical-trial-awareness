namespace :users do

  desc 'Creates an admin user'
  task create_admin: :environment do
    User.new({
      email: 'admin@ctvolunteers.org',
      admin: true,
      password: 'ctvolunteersadmin',
      password_confirmation: 'ctvolunteersadmin'
    }).save(validate: false)
  end

end

