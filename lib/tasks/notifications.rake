desc 'Sends email notifications of matching trials to profile holders'
task send_notifications: :environment do
  Profile.active.each do |profile|
    trials = profile.trials_needing_notification.to_a
    if trials.first
      UserMailer.trial_notification(profile, trials).deliver_later!(wait: 1.second)
      trials.each do |trial|
        TrialNotification.create(profile_id: profile.id, dartmouth_clinical_trial_id: trial.id)
      end
    end
  end
end

desc 'Dev only. Test notification email'
task send_test_notification: :environment do
  profile = Profile.first
  trials = DartmouthClinicalTrial.where(healthy_volunteers: true).all.to_a
  UserMailer.trial_notification(profile, trials).deliver_later!(wait: 1.second)
end
