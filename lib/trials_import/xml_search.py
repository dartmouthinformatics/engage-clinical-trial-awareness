from dateutil.parser import parse
import os


def find_nth_child(root, parent_node, child_node, index):
    """ Finds the nth child node based on index or None if not found """
    out_str = None
    if root.findall(parent_node):
        if root.findall(parent_node)[0].findall(child_node):
            if len(root.findall(parent_node)[0].findall(child_node)) > (index - 1):
                out_str = root.findall(parent_node)[0].findall(child_node)[index - 1].text
    return out_str

def find_nth(root, node, index):
    """ Finds the nth node based on index or None if not found """
    out_str = None
    found_node_count = 0
    if root.findall(node):
        if len(root.findall(node)) > (index - 1):
            out_str = root.findall(node)[index - 1].text
    return out_str

def find_date(root, node):
    """ Parses dates such as 'May 2012' and formats them for INSERT """
    if root.findall(node):
        text = root.find(node).text
        # Parse date. Returns the 21st of the month for some reason, so set to 1st
        return "'" + str(parse(text)).replace('-21 ', '-01 ') + "',"
    else:
        return None

def find_attr(root, node, attr):
    """ Finds the value for a node attribute """
    if root.findall(node):
        if attr in root.find(node).attrib:
            text = root.find(node).attrib[attr]
            if text != None:
                return text
    return None

def find(root, node):
    """ Finds the text for a node directly below the root node """
    if root.findall(node):
        text = root.find(node).text
        return text
    else:
        return None

def find_child(root, parent_node, child_node):
    """ Finds the text for a node directly two below the root node """
    if root.findall(parent_node):
        if root.findall(parent_node)[0].findall(child_node):
            text = root.find(parent_node).find(child_node).text
            if text == 'N/A':
                return None
            return strip_empty(text)
    return None

def find_grandchild(root, parent_node, child_node, grandchild_node):
    """ Finds the text for a node directly three below the root node """
    if root.findall(parent_node):
        if root.findall(parent_node)[0].findall(child_node):
            if root.findall(parent_node)[0].findall(child_node)[0].findall(grandchild_node):
                text = root.find(parent_node).find(child_node).find(grandchild_node).text
                return text
    return None

def strip_empty(text):
    """ http://stackoverflow.com/questions/1140958/whats-a-quick-one-liner-to-remove-empty-lines-from-a-python-string """ 
    return os.linesep.join([s.lstrip(' ') for s in text.splitlines() if s])
