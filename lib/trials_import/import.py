""" Database importer and classifier for clinicaltrials.gov

This code scrapes clinical trial information from clinicaltrials.gov
and inserts it into a Postgres database. User-based Postgres authentication is assumed.

New records are added only if there is not already a db record with a given nct_id.
Existing db records are checked against downloaded records to ensure correct status.

Each record is classified into one of 58 categories, using a Naive Bayes classifier.

This program expects a directory called 'xml' to exist at the same path as this file.
Will Haslett 21-Sep-15

"""
from sklearn.externals import joblib
from numpy import array
import urllib
import zipfile
import os, re
import xml.etree.ElementTree as ET
import psycopg2
import sys
import datetime
from db import connect_to_db
from xml_search import *

CLF = joblib.load('saved_nb.pkl') 
VECTORIZER = joblib.load('saved_vectorizer.pkl')

(pg_connection, pg_cursor) = connect_to_db()

# schema definition
CREATE_TABLE_STR = (
    "CREATE TABLE IF NOT EXISTS dartmouth_clinical_trials(id SERIAL PRIMARY KEY," 
    "category            TEXT,"
    "verified            BOOLEAN DEFAULT FALSE,"
    "created_at          TIMESTAMP,"
    "url                 TEXT,"
    "nct_id              TEXT,"
    "title               TEXT,"
    "sponsor             TEXT,"
    "summary             TEXT,"
    "status              TEXT,"
    "start_date          DATE,"
    "end_date            DATE,"
    "end_date_status     TEXT,"
    "primary_outcome     TEXT,"
    "condition_1         TEXT,"
    "condition_2         TEXT,"
    "condition_3         TEXT,"
    "condition_4         TEXT,"
    "condition_5         TEXT,"
    "gender              TEXT,"
    "min_age             TEXT,"
    "max_age             TEXT,"
    "min_age_sec         BIGINT,"
    "max_age_sec         BIGINT,"
    "healthy_volunteers  BOOLEAN,"
    "eligibility         TEXT,"
    "study_official_name TEXT,"
    "contact_name        TEXT,"
    "contact_phone       TEXT,"
    "contact_email       TEXT,"
    "alt_contact_name    TEXT,"
    "alt_contact_phone   TEXT,"
    "alt_contact_email   TEXT,"
    "keyword_1           TEXT,"
    "keyword_2           TEXT,"
    "keyword_3           TEXT,"
    "keyword_4           TEXT,"
    "keyword_5           TEXT,"
    "mesh_term_1         TEXT,"
    "mesh_term_2         TEXT,"
    "mesh_term_3         TEXT,"
    "mesh_term_4         TEXT,"
    "mesh_term_5         TEXT)"
)

# Should not be called in production
def drop_table():
    pg_cursor.execute("DROP TABLE IF EXISTS dartmouth_clinical_trials")
    pg_connection.commit()

def create_table():
    #drop_table()
    pg_cursor.execute(CREATE_TABLE_STR)
    pg_connection.commit()

def download_xml():
    """ Download current search results from clinicaltrials.gov """
    # Delete existing files in XML directory
    for file_name in os.listdir('xml'):
        if file_name != '.gitkeep':
            os.remove(os.path.join('xml', file_name))
    try:
        # Dartmouth recruiting in NH or VT:
        urllib.urlretrieve("https://clinicaltrials.gov/ct2/results?term=dartmouth+hitchcock&recr=Recruiting&no_unk=Y&state1=NA%3AUS%3AVT&state2=NA%3AUS%3ANH&resultsxml=true", "./xml/trials.zip")
    except Exception as e:
        # Print error and exit if download was not successful
        print('Error %s' % e)
        sys.exit(1)
    # Extract zipfile
    with zipfile.ZipFile('xml/trials.zip', 'r') as zip_file:
        zip_file.extractall('xml')
    # Delete zip file
    os.remove(os.path.join('xml/trials.zip'))

def process_xml():
    """ Process XML files """
    nct_ids = []
    for file_name in os.listdir('xml'):
        if file_name != '.gitkeep':
            try:
                tree = ET.parse(os.path.join('xml', file_name))
            except:
                print('Could not open ' + file_name)
                continue
            root = tree.getroot()
            nct_ids.append(find_child(root, 'id_info', 'nct_id'))
            if not exists_in_database(root):
                insert_query = "INSERT INTO dartmouth_clinical_trials VALUES(default, %s, default, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                insert_list = build_insert(root)
                pg_cursor.execute(insert_query, insert_list)
                pg_connection.commit()
    check_db_records(nct_ids)

def local_contact(root):
    """ Looks for a location/facility that has Dartmouth in its name. Returns contact details if found. """
    contact_dict = {
        'name': None,
        'phone': None,
        'email': None
    }
    for location in root.findall('location'):
        for facility in location.findall('facility'):
            name = facility.find('name')
            if name is not None and 'dartmouth' in name.text.lower():
                contact = location.find('contact')
                if contact is not None:
                    contact_name_node = contact.find('last_name')
                    if contact_name_node is not None: contact_dict['name'] = contact_name_node.text
                    contact_phone_node = contact.find('phone')
                    if contact_phone_node is not None: contact_dict['phone'] = contact_phone_node.text
                    contact_email_node = contact.find('email')
                    if contact_email_node is not None: contact_dict['email'] = contact_email_node.text
                    return contact_dict
    return contact_dict

def check_db_records(nct_ids):
    """ Checks all existing records and sets status based on latest download """
    pg_cursor.execute("SELECT * FROM dartmouth_clinical_trials")
    recruiting_records = pg_cursor.fetchall()
    for row in recruiting_records:
        # NOTE: This is brittle, but I don't know another way to retrive the columns
        nct_id = row[5]
        status = row[9]
        if status == 'Recruiting':
            if nct_id not in nct_ids:
                print('Stale record: Setting ' + nct_id + ' to inactive status')
                pg_cursor.execute("UPDATE dartmouth_clinical_trials SET status = 'Inactive' where nct_id = %s", [nct_id])
                pg_connection.commit()
        if status == 'Inactive':
            if nct_id in nct_ids:
                print('Reactivated record: Setting ' + nct_id + ' to recruiting status')
                pg_cursor.execute("UPDATE dartmouth_clinical_trials SET status = 'Recruiting' where nct_id = %s", [nct_id])
                pg_connection.commit()

def exists_in_database(root):
    """ Returns a boolean indicating whether or not the passed-in XML record already exists in the db """
    nct_id = find_child(root, 'id_info', 'nct_id')
    select_query = "SELECT * FROM dartmouth_clinical_trials WHERE nct_id = %s"
    select_list = [nct_id]
    pg_cursor.execute(select_query, select_list)
    if pg_cursor.fetchone():
        return True
    return False

def bag_of_words(root):
    """ Concatenates relevant attributes into a bag of words for the classifier """
    bow = []
    bow.append(none_to_empty_str(find(root, 'official_title')).encode('utf-8'))
    bow.append(none_to_empty_str(find_child(root, 'brief_summary', 'textblock')).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth(root, 'condition', 1)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth(root, 'condition', 2)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth(root, 'condition', 3)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth(root, 'condition', 4)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth(root, 'condition', 5)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth(root, 'keyword', 1)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth(root, 'keyword', 2)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth(root, 'keyword', 3)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth(root, 'keyword', 4)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth(root, 'keyword', 5)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth_child(root, 'condition_browse', 'mesh_term', 1)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth_child(root, 'condition_browse', 'mesh_term', 2)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth_child(root, 'condition_browse', 'mesh_term', 3)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth_child(root, 'condition_browse', 'mesh_term', 4)).encode('utf-8'))
    bow.append(none_to_empty_str(find_nth_child(root, 'condition_browse', 'mesh_term', 5)).encode('utf-8'))
    return ' '.join(bow)

def none_to_empty_str(obj):
    if obj == None:
        return ''
    return obj


def category(bow):
    """ Applies a saved Naive Bayes classifier to the passed-in bag of words """
    bow_array = array([bow])
    bow_vector = VECTORIZER.transform(bow_array)
    category = CLF.predict(bow_vector)
    return str(category[0])

def build_insert(root):
    """ Build SQL INSERT list for a given study """
    insert_list = []
    insert_list.append(None)
    insert_list.append('{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now()))
    insert_list.append(find_child(root, 'required_header', 'url'))
    insert_list.append(find_child(root, 'id_info', 'nct_id'))
    insert_list.append(find(root, 'official_title'))
    insert_list.append(find_grandchild(root, 'sponsors', 'lead_sponsor', 'agency'))
    insert_list.append(find_child(root, 'brief_summary', 'textblock'))
    insert_list.append(find(root, 'overall_status'))
    insert_list.append(find_date(root, 'start_date') )
    insert_list.append(find_date(root, 'completion_date') )
    insert_list.append(find_attr(root, 'completion_date', 'type'))
    insert_list.append(find_child(root, 'primary_outcome', 'measure'))
    insert_list.append(find_nth(root, 'condition', 1))
    insert_list.append(find_nth(root, 'condition', 2))
    insert_list.append(find_nth(root, 'condition', 3))
    insert_list.append(find_nth(root, 'condition', 4))
    insert_list.append(find_nth(root, 'condition', 5))
    insert_list.append(find_child(root, 'eligibility', 'gender'))
    min_age = find_child(root, 'eligibility', 'minimum_age') 
    insert_list.append(min_age)
    max_age = find_child(root, 'eligibility', 'maximum_age')
    insert_list.append(max_age)
    insert_list.append(sec_duration(min_age))
    insert_list.append(sec_duration(max_age))
    insert_list.append(find_child(root, 'eligibility', 'healthy_volunteers').replace('No', 'False').replace('Accepts Healthy Volunteers', 'True'))
    insert_list.append(find_grandchild(root, 'eligibility', 'criteria', 'textblock'))
    insert_list.append(find_child(root, 'overall_official', 'last_name'))

    # Default contact is overall study contact
    contact_name = find_child(root, 'overall_contact', 'last_name')
    contact_phone = find_child(root, 'overall_contact', 'phone')
    contact_email = find_child(root, 'overall_contact', 'email')

    # Contact info becomes Dartmouth location contact info if available.
    local_contact_dict = local_contact(root)
    if local_contact_dict['name'] is not None: contact_name = local_contact_dict['name']
    if local_contact_dict['phone'] is not None: contact_phone = local_contact_dict['phone']
    if local_contact_dict['email'] is not None: contact_email = local_contact_dict['email']

    insert_list.append(contact_name)
    insert_list.append(contact_phone)
    insert_list.append(contact_email)

    insert_list.append(find_child(root, 'overall_contact_backup', 'last_name'))
    insert_list.append(find_child(root, 'overall_contact_backup', 'phone'))
    insert_list.append(find_child(root, 'overall_contact_backup', 'email'))
    insert_list.append(find_nth(root, 'keyword', 1))
    insert_list.append(find_nth(root, 'keyword', 2))
    insert_list.append(find_nth(root, 'keyword', 3))
    insert_list.append(find_nth(root, 'keyword', 4))
    insert_list.append(find_nth(root, 'keyword', 5))
    insert_list.append(find_nth_child(root, 'condition_browse', 'mesh_term', 1))
    insert_list.append(find_nth_child(root, 'condition_browse', 'mesh_term', 2))
    insert_list.append(find_nth_child(root, 'condition_browse', 'mesh_term', 3))
    insert_list.append(find_nth_child(root, 'condition_browse', 'mesh_term', 4))
    insert_list.append(find_nth_child(root, 'condition_browse', 'mesh_term', 5))
    return insert_list

def sec_duration(age_str):
    if age_str is None:
        return None
    try:
        [dur, units] = str.split(age_str)
        dur = int(dur)
        units = units.lower()
        # Default is years (seconds per year)
        # Only handles Year(s) and Month(s) as of now
        mult = None
        if 'year' in units:
            mult = 3.154e+7
        if 'month' in units:
            mult = 3.154e+7 / 12
        if mult:
            return dur * mult
        else:
            return None
    except Exception as e:
        print(str(e))
        return None

def export_csv():
    file_name = os.path.dirname(os.path.realpath(__file__)) + '/dartmouth_clinical_trials.csv'
    csv_sql = "Copy (Select * From dartmouth_clinical_trials) To '" + file_name + "' With (HEADER, FORMAT CSV, FORCE_QUOTE *);"
    pg_cursor.execute(csv_sql)
    
""" Main routine """
print('Making sure table exists')
create_table()
print('Downloading XML from clinicaltrials.gov')
download_xml()
print('Parsing XML and creating database records')
process_xml()
# print('Exporting CSV file')
# export_csv()
pg_connection.close()
