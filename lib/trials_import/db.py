import yaml
import psycopg2

def connect_to_db():
    # Open and parse Rails database yaml file
    with open("../../config/database.yml", 'r') as stream:
        try:
            yml = yaml.load(stream)
            environment = yml.keys()[0]
            db = yml[environment]['database']
            user = yml[environment]['username']

            pwd = None
            if 'password' in yml[environment]:
                pwd = yml[environment]['password']

            host = None
            if 'host' in yml[environment]:
                host = yml[environment]['host']

        except yaml.YAMLError as exc:
            print(exc)

    # Connect to the database
    try:
        # Database connection
        pg_connection = psycopg2.connect(database=db, user=user, password=pwd, host=host)
        # Database cursor
        pg_cursor = pg_connection.cursor()
    except psycopg2.DatabaseError as e:
        print('Error %s' % e)
        sys.exit(1)
    return (pg_connection, pg_cursor)
